# httphealth

Simple Go application for executing health-check command and serving
response on host as HTTP.

## Usage

`httphealth` is using environment variables for settings:

- `HEALTHCHECK_CMD` - command as string (required)
- `HEALTHCHECK_PORT` - TCP port where application should listen (optional, default `1985`)

It can be run as a oneliner

```bash
HEALTHCHECK_CMD="uname -a" HEALTHCHECK_PORT=2019 httphealth
```

which will listen on all interfaces of host on port `2019` and execute
`uname -a` on the host and serve response on route `/health`. If you run
it locally you should see response on
[localhost:2019/health](http://localhost:2019/health)

### Expose in Docker

To be able to run `httphealth` alongside of your main process you need
to create an entrypoint script that will do this job.

```shell
#!/bin/sh

# Exit immediately if any command or pipeline of commands fails
set -o errexit

# Start healthcheck service in backgound
/httphealth&

# Start main process
/my_app
```

In your Dockerfile you need to set required ENV and copy `httphealth` binary and your entrypoint shell script.
