package main

import (
	"net/http"
	"net/http/httptest"
	"os/exec"
	"reflect"
	"testing"
)

func Test_getHealthPort(t *testing.T) {
	tests := []struct {
		name    string
		s       string
		want    int
		wantErr bool
	}{
		{"empty string should return default 1985", "", 1985, false},
		{"string 2000 should return int 2000", "2000", 2000, false},
		{"string below 1024 should return 0 and error", "80", 0, true},
		{"string above 65535 should return 0 and error", "70000", 0, true},
		{"string FOO should return 0 and error", "FOO", 0, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := getHealthPort(tt.s)
			if (err != nil) != tt.wantErr {
				t.Errorf("getHealthPort() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("getHealthPort() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_newHealthcheck(t *testing.T) {

	tests := []struct {
		name           string
		healthcheckCMD string
		wantCmd        *exec.Cmd
		wantErr        bool
	}{
		{"success from one word command", "uname", exec.Command("uname"), false},
		{"success from two words command", "uname -a", exec.Command("uname", "-a"), false},
		{"fail for empty command", "", nil, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := newHealthcheck(tt.healthcheckCMD)
			if (err != nil) != tt.wantErr {
				t.Errorf("newHealthcheck() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			var want *healthckeck
			if tt.wantCmd != nil {
				want = &healthckeck{Cmd: *tt.wantCmd}
			}
			if !reflect.DeepEqual(got, want) {
				t.Errorf("newHealthcheck() = %v, want %v", got, want)
			}
		})
	}
}

func Test_healthckeck_healthHandler(t *testing.T) {
	tests := []struct {
		name       string
		Cmd        *exec.Cmd
		wantStatus int
		wantBody   string
	}{
		{"success for uname", exec.Command("uname"), http.StatusOK, "Linux\n"},
		{"fail for for 'ls non-sense'", exec.Command("ls", "non-sense"), http.StatusInternalServerError, "healthckeck command failed: ls: cannot access 'non-sense': No such file or directory\n"},
		{"fail for NONSENSE", exec.Command("NONSENSE"), http.StatusInternalServerError, "execution of healthckeck failed: exec: \"NONSENSE\": executable file not found in $PATH"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			h := &healthckeck{
				Cmd: *tt.Cmd,
			}

			// Create a request to pass to our handler. We don't have any query parameters for now, so we'll
			// pass 'nil' as the third parameter.
			req, err := http.NewRequest("GET", "/health", nil)
			if err != nil {
				t.Fatal(err)
			}

			// We create a ResponseRecorder (which satisfies http.ResponseWriter) to record the response.
			rr := httptest.NewRecorder()
			handler := http.HandlerFunc(h.healthHandler)

			// Our handlers satisfy http.Handler, so we can call their ServeHTTP method
			// directly and pass in our Request and ResponseRecorder.
			handler.ServeHTTP(rr, req)

			// Check the status code is what we expect.
			if status := rr.Code; status != tt.wantStatus {
				t.Errorf("handler returned wrong status code: got %v want %v",
					status, tt.wantStatus)
			}

			// Check the response body is what we expect.
			if rr.Body.String() != tt.wantBody {
				t.Errorf("handler returned unexpected body: got %v want %v",
					rr.Body.String(), tt.wantBody)
			}
		})
	}
}
