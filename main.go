package main

import (
	"fmt"
	"net/http"
	"os"
	"os/exec"
	"strconv"

	"github.com/google/shlex"
)

var (
	version = "dev"
	commit  = "none"
	date    = "unknown"
)

func main() {
	if err := run(); err != nil {
		fmt.Fprintf(os.Stderr, "ERROR: %v\n", err)
		os.Exit(1)
	}
}

func run() error {
	fmt.Printf("httphealth %v-%v, built at %v\n", version, commit, date)
	h, err := newHealthcheck(os.Getenv("HEALTHCHECK_CMD"))
	if err != nil {
		return fmt.Errorf("cannot create new healthcheck from env HEALTHCHECK_CMD: %v", err)
	}
	healthPort, err := getHealthPort(os.Getenv("HEALTHCHECK_PORT"))
	if err != nil {
		return err
	}
	fmt.Printf("command: %s, port: %d\n", h.String(), healthPort)
	http.HandleFunc("/health", h.healthHandler)
	if err := http.ListenAndServe(fmt.Sprintf(":%d", healthPort), nil); err != nil {
		return fmt.Errorf("cannot run server: %v", err)
	}
	return nil
}

type healthckeck struct {
	exec.Cmd
}

func newHealthcheck(healthcheckCMD string) (*healthckeck, error) {
	if healthcheckCMD == "" {
		return nil, fmt.Errorf("command string is empty")
	}
	cmdLine, err := shlex.Split(healthcheckCMD)
	if err != nil {
		return nil, fmt.Errorf("cannot parse as command: %v", err)
	}
	h := &healthckeck{}
	if len(cmdLine) > 1 {
		h.Cmd = *exec.Command(cmdLine[0], cmdLine[1:]...)
	} else {
		h.Cmd = *exec.Command(cmdLine[0])
	}
	return h, nil
}

func (h healthckeck) healthHandler(w http.ResponseWriter, req *http.Request) {
	out, err := h.Output()
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		// Command failed because of an unsuccessful exit code
		if exitError, ok := err.(*exec.ExitError); ok {
			fmt.Fprintf(w, "healthckeck command failed: %s", exitError.Stderr)
			return
		}
		// Execution failed (eg. command not in PATH)
		fmt.Fprintf(w, "execution of healthckeck failed: %v", err)
		return
	}
	w.WriteHeader(http.StatusOK)
	fmt.Fprintf(w, "%s", out)
}

func getHealthPort(s string) (int, error) {
	// Set to default if custom is not required
	if s == "" {
		return 1985, nil
	}
	healthPort, err := strconv.Atoi(s)
	if err != nil {
		return 0, fmt.Errorf("cannot parse int from %s: %v", s, err)
	}
	if healthPort < 1024 || healthPort > 65535 {
		return 0, fmt.Errorf("port number has to be between 1023 and 65535")
	}
	return healthPort, nil
}
